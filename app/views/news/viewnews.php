<div class="news-container">
    <div class="news-container-data">
        <div class="news-holder">
            <div class="title">
                <?php echo $data['news']->title; ?>
            </div>
            <div class="publish">
                <?php echo $data['news']->created_at; ?>
            </div>
            <div class="context">
                <?php echo $data['news']->body; ?>
            </div>
            <hr>
            <div class="response">
                <h3>Write a response</h3>
                <form action="<?php echo BASEURL; ?>/comment/add" method="POST">
                    <input type="text" name="news_id" value="<?php echo $data['news']->id; ?>">
                    <textarea name="body" placeholder="Add some text..."></textarea>
                    <input type="submit" value="Submit">
                    <span><?php echo $data['body_err']; ?></span>
                </form>
            </div>
            <?php foreach($data['comment'] as $comment) : ?>
                <div class="comment-holder">                
                    <img src="/img/avatar.png" alt="">
                    <div class="comment-context">
                        <?php echo $comment->body; ?>
                    </div>
                    <div class="comment-context-other">
                        <ul>
                            <li>Posted: <?php echo $comment->created_at; ?></li>
                            <li>                                
                                <form action="<?php echo BASEURL.'/comment/delete/'.$comment->id; ?>" method="post">
                                    <input type="submit" value="Delete">
                                </form>
                            </li>
                        </ul>
                    </div>                
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>