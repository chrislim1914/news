<?php

class AdminController extends Controller{

    // instantiate MODELS
    public function __construct(){
        $this->newsModel = $this->model('News');
        $this->commentModel = $this->model('Comment');
    }

    public function getNewsforUpdate($id) {
        $view = $this->newsModel->getNewsById($id);
            $data = [
                'id'        => $view->id,
                'title'     => $view->title,
                'body'      => $view->body,
                'title_err' => '',
                'body_err'  => ''
            ];
        $this->view('admin/update', $data);
    }

    public function index() {
      // get news
      $news = $this->newsModel->getAllNews();

      // array to put our data
      $newnews = [];
      // loop through News
      foreach($news as $newsdata) {
        $count = $this->commentModel->getCountByNewsID($newsdata->id);
        $newnews[] = [
          'id'          => $newsdata->id,
          'title'       => $newsdata->title,
          'created_at'  => $newsdata->created_at,
          'body'        => $newsdata->body,
          'count'       => $count
        ];
      }
      
      $data = [
        'news' => $newnews
      ];
        
      $this->view('admin/index', $data);
    }

    public function newsDelete($id) {
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //Execute
            if($this->newsModel->deleteNews($id)){
                redirect('admin/index');
            } else {
                die('Something went wrong');
            }
        } else {
            redirect('admin/index');
        }
    }

    public function newsUpdate($id) {
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // Sanitize POST
            $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
            $data = [
                'id'    => $id,
                'title' => trim($_POST['title']),
                'body'  => trim($_POST['body']),
                // add created_at heres
            ];
            
            // Validate comment
            if(empty($data['title']) || empty($data['body'])){
                $data['title_err'] = empty($data['title']) ? 'Please enter title of news' : '';
                $data['body_err']  = empty($data['body']) ? 'Please enter context of news' : '';
            }else{
                var_dump($data);
                if($this->newsModel->updateNews($data)){
                    redirect('admin');
                } else {
                    die('Something went wrong');
                }
            }
                $this->view('admin/update', $data);
  
        } else {            
            $view = $this->newsModel->getNewsById($id);
            $data = [
                'id'        => $view->id,
                'title'     => $view->title,
                'body'      => $view->body,
                'title_err' => '',
                'body_err'  => ''
            ];
            $this->view('admin/update', $data);
        }
    }

    public function newsCreate() {
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // Sanitize POST
            $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
            $data = [
                'title' => trim($_POST['title']),
                'body'  => trim($_POST['body']),
                // add created_at heres
            ];
            
            // Validate comment
            if(empty($data['title']) || empty($data['body'])){
                $data['title_err'] = empty($data['title']) ? 'Please enter title of news' : '';
                $data['body_err']  = empty($data['body']) ? 'Please enter context of news' : '';
            }else{
                if($this->newsModel->addNews($data)){
                    redirect('admin');
                } else {
                    die('Something went wrong');
                }
            }
                $this->view('admin/create', $data);
  
        } else {
            $data = [
                'title' => '',
                'title_err' => '',
                'body_err'  => ''
            ];
            
            $this->view('admin/create',$data);
        }
    }
}