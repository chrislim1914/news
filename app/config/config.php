<?php
// DB Params
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "newstest");

// App Root
define('APPROOT', dirname(dirname(__FILE__)));
define('APPDOC', dirname(dirname(dirname(__FILE__))));
// URL Root
define('BASEURL', 'http://localhost:8080');
// define('CURRENT_DATE_TIME', new date());
// echo CURRENT_DATE_TIME;