<?php
  class Comment {
    private $db;
    
    public function __construct(){
        $this->db = new Database;
    }

    public function getCountByNewsID($newsid) {
        $commentcount = $this->db->query("SELECT COUNT(news_id)as `count` FROM `comment` WHERE news_id =:newsid");  
        $this->db->bind(':newsid', $newsid);
        
        $row = $this->db->single();
        return $row->count;
    }

    public function getCommentByID($newsid) {
        $commentlist = $this->db->query("SELECT * FROM `comment` WHERE news_id =:newsid ORDER BY id DESC");
        $this->db->bind(':newsid', $newsid);
        
        $row = $this->db->resultset();
        return $row;
    }

    // Add Comment
    public function addComment($data){
        // Prepare Query
        $this->db->query('INSERT INTO comment (body, news_id) 
        VALUES (:body, :news_id)');

        // Bind Values
        $this->db->bind(':body', $data['body']);
        $this->db->bind(':news_id', $data['news_id']);
        
        //Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    public function deleteComments($id){
        // Prepare Query
        $this->db->query('DELETE FROM comment WHERE id = :id');

        // Bind Values
        $this->db->bind(':id', $id);
        
        //Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        } 
    }

    public function getNewsIDinComment($id) {
        $newsid = $this->db->query("SELECT news_id FROM `comment` WHERE id =:id");
        $this->db->bind(':id', $id);
        
        $row = $this->db->resultset();
        return $row;
    }
}