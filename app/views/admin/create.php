<div class="news-container">
    <div class="admin-news-container-data">
        <?php include APPROOT.'/views/include/sidebar.php'; ?>
        <div class="table-holder">
            <div class="table-head">
                <ul>
                    <li>Create New News</li>
                    <li>
                        <a href="<?php echo BASEURL; ?>/admin">Back</a>
                    </li>
                </ul>
            </div>
            <div class="table-data">
                <form action="<?php echo BASEURL.'/admin/newsCreate' ?>" method="post">
                    <label class="form-title" for="title">Title:</label>
                    <input type="text" name="title" value="<?php echo $data['title']; ?>">
                    <span><?php echo $data['title_err']; ?></span>
                    <label class="form-title" for="body">Context:</label>
                    <textarea name="body"></textarea>
                    <span><?php echo $data['body_err']; ?></span>
                    <input type="submit" Value="Submit">
                </form>
            </div>
        </div>
    </div>
</div>