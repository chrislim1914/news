<?php

class Controller {
    
    /**
     * method to load model
     * 
     * @param $model
     * @return new $model
     */
    public function model($model){
        // Require model file
        require_once '../app/models/' . $model . '.php';
        // Instantiate model
        return new $model();
    }
  
    /**
     * method to load view
     * 
     * @param $url, $data []
     */
    public function view($url, $data = []){
        // Check for view file
        if(file_exists('../app/views/'.$url.'.php')){
            // Require view file
            require_once '../app/views/'.$url.'.php';
        } else {
            // No view exists
            die('View does not exist');
        }
    }
}