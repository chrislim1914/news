<?php

class CommentController extends Controller{

    public function __construct(){
      $this->newsModel = $this->model('News');
      $this->commentModel = $this->model('Comment');
    }

    // Add Comment
    public function add(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // Sanitize POST
            $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
            $data = [
                'body' => trim($_POST['body']),
                'news_id' => trim($_POST['news_id'])
            ];
            
            // Validate comment
            if(empty($data['body'])){
                $data['body_err'] = 'Please enter the comment';
            }
  
            // Make sure there are no errors
            if(empty($data['body_err'])){
                // Validation passed
                //Execute
                if($this->commentModel->addComment($data)){
                    redirect('news/show/'.$_POST['news_id']);
                } else {
                    die('Something went wrong');
                }
            } else {                
                redirect('news/show/'.$_POST['news_id']);
            }
  
        } else {
            $data = [
                'body' => '',
            ];
  
          $this->view('posts/add', $data);
        }
    }

    //delete comment
    public function delete($id) {
        $getnewsid = $this->commentModel->getNewsIDinComment($id);
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //Execute 
            if($this->commentModel->deleteComments($id)){
                redirect('news/show/'.$getnewsid[0]->news_id);
            } else {
                die('Something went wrong');
            }
        } else {
            redirect('news/show/'.$getnewsid[0]->news_id);
        }
    }
}