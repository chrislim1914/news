<?php

class NewsController extends Controller{
  
    public function __construct(){
      $this->newsModel = $this->model('News');
      $this->commentModel = $this->model('Comment');
    }

    public function index() {
      // get news
      $news = $this->newsModel->getAllNews();

      // array to put our data
      $newnews = [];
      // loop through News
      foreach($news as $newsdata) {
        $count = $this->commentModel->getCountByNewsID($newsdata->id);
        $newnews[] = [
          'id'          => $newsdata->id,
          'title'       => $newsdata->title,
          'created_at'  => $newsdata->created_at,
          'body'        => $newsdata->body,
          'count'       => $count
        ];
      }
      
      $data = [
        'news' => $newnews
      ];
        
      $this->view('news/index', $data);
    }

    public function show($id) {
      $view = $this->newsModel->getNewsById($id);
      $comment = $this->commentModel->getCommentByID($id);
      $data = [
        'news' => $view,
        'comment' => $comment,        
        'body_err'  => ''
      ];
      $this->view('news/viewnews', $data);
    }
}
