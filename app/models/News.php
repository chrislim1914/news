<?php

  class News {
    private $db;
    
    public function __construct() {
        $this->db = new Database;
    }

    // Get All Posts
    public function getAllNews() {
        $this->db->query("SELECT * FROM `news` ORDER BY id DESC");

        // get all news
        $newsdata = $this->db->resultset();
        return $newsdata;
    }

    // Get News By ID
    public function getNewsById($id) {
        $this->db->query("SELECT * FROM `news` WHERE id = :id");

        $this->db->bind(':id', $id);
        
        $row = $this->db->single();

        return $row;
    }
    
    // Add News
    public function addNews($data){
        // Prepare Query
        $this->db->query('INSERT INTO news (title, body) 
        VALUES (:title, :body)');
  
        // Bind Values
        $this->db->bind(':title', $data['title']);
        $this->db->bind(':body', $data['body']);
        
        //Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    // Delete News
    public function deleteNews($id){
        // Prepare Query
        $this->db->query('DELETE FROM news WHERE id = :id');

        // Bind Values
        $this->db->bind(':id', $id);
        
        //Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        } 
    }

    // update News
    public function updateNews($data){
        // Prepare Query
        $this->db->query('UPDATE news SET title = :title, body = :body WHERE id = :id');
        // Bind Values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':title', $data['title']);
        $this->db->bind(':body', $data['body']);
        
        //Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }
  }