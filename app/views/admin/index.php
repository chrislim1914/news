<div class="news-container">
    <div class="admin-news-container-data">
        <?php include APPROOT.'/views/include/sidebar.php'; ?>
        <div class="table-holder">
            <div class="table-head">
                <ul>
                    <li>News Management</li>
                    <li>
                        <a href="<?php echo BASEURL; ?>/admin/newsCreate">Create</a>
                    </li>
                </ul>
            </div>
            <div class="table-data">
            <?php foreach($data['news'] as $allnews) : ?>
                <ul>
                    <li><?php echo $allnews['title']; ?></li>
                    <li><?php echo $allnews['created_at']; ?></li>
                    <li><?php echo $allnews['body']; ?></li>
                    <li>
                        <form action="<?php echo BASEURL.'/admin/getNewsforUpdate/'.$allnews['id']; ?>" method="post">
                            <input type="submit" value="Edit">
                        </form>
                        <form action="<?php echo BASEURL.'/admin/newsDelete/'.$allnews['id']; ?>" method="post">
                            <input type="submit" value="Delete">
                        </form>
                    </li>
                </ul>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>