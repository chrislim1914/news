-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2020 at 08:19 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newstest`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) NOT NULL,
  `body` mediumtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `news_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `body`, `created_at`, `news_id`) VALUES
(1, 'this is a response comment', '2020-01-19 06:12:52', 1),
(2, 'Another Comment', '2020-01-19 06:25:40', 1),
(4, 'According to Peter I. PÂ´ apicsÂ´ 1 ,2, he gave a detailed description of selected activity phenomena on the sun: Solar Prominences and in connection with the solar flares and CMEs or Coronal Mass Ejections.', '2020-01-19 06:43:50', 1),
(14, 'sadasd', '2020-01-19 07:11:49', 5),
(16, 'Prominences have been observed for decades, but they are still not well understood.', '2020-01-19 07:14:14', 6),
(17, 'Itâ€™s still unknown, how they form and provide support against gravity for the enhanced density in the corona.', '2020-01-19 07:15:45', 6),
(18, 'It can be observed in a Wide wavelength range corresponding to the temperature of their plasma and of the surrounding volume.', '2020-01-19 07:16:02', 6),
(19, 'Flares or Solar Flares are also phenomena on the sun; they are a violent explosion that takes place in the chromosphere. But the Strongest ones can heat up even the photosphere that they produce Flares that are visible even in white light.', '2020-01-19 07:16:17', 6),
(20, 'In this task, I gave the summarization of the Research that I have studied. I included some of the information that I have read and understand about the most important activity phenomena of the sun above the photosphere â€“ prominences, solar flares and coronal mass ejections. I also included some of the basic observational characteristics, short history of each activity and some results from the previous observations from the previous people who discovered each activity. The main reason why we need this kind of research is that making better space weather predictions is crucial for the stability of our strongly technology dependent society.', '2020-01-19 07:16:29', 6);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) NOT NULL,
  `title` varchar(511) DEFAULT NULL,
  `body` mediumtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `body`, `created_at`) VALUES
(5, 'Sample News title', 'This is Description', '2020-01-19 05:57:01'),
(6, 'Tremendous Prominences', 'According to Peter I. PÂ´ apicsÂ´ 1 ,2, he gave a detailed description of selected activity phenomena on the sun: Solar Prominences and in connection with the solar flares and CMEs or Coronal Mass Ejections. He also discussed some of the basic facts about these phenomena focusing only on: Solar Prominences, the history of observations from the beginning to the most recent observations of the latest space probes, the classifications, and the most important observational characteristics of each. He also included the origin, and the role of Thermal Instability, some of the corresponding magneto hydrodynamics models and simulations and also the summary about the relation between prominences, solar flares and also CMEs.', '2020-01-19 07:12:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
