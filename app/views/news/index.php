<div class="news-container">
    <div class="news-container-data">            
        <?php foreach($data['news'] as $allnews) : ?>
            <a href="<?php echo BASEURL; ?>/news/show/<?php echo $allnews['id']; ?>">  
                <div class="news-holder">
                    <div class="title">
                        <?php echo $allnews['title']; ?>
                    </div>
                    <div class="publish">
                        <?php echo $allnews['created_at']; ?>
                    </div>
                    <div class="context">
                        <?php echo $allnews['body']; ?>
                    </div>
                    <div class="comment-count">
                        <img src="/img/comment.png" alt="comment logo">
                        <span><?php echo $allnews['count']; ?></span>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
</div>