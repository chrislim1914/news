<?php
    require_once('../app/bootstrap.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
    <title>Rockstar News</title>
    </head>
<body>
    <div class="header-top">
        <div class="logo">
            <a href="<?php echo BASEURL; ?>">
                <img src="/img/news-logo.png" >
            </a>
        </div>
        <div class="strip">
            <a href="<?php echo BASEURL."/admin"; ?>">
                News Management
            </a>
        </div>
    </div>
    <hr>
<?php
    $core = new Core();
?>

    </body>
</html>